<?php


interface SubscriberInterface
{

	/**
	 * @return array
	 */
	public function registerEvent();

}
