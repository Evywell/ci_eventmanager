EventManager pour Codeigniter (3.1.7)
===

Fonctionnalités disponibles:

 1. Ecouter un événement
 2. Déclencher un événement
 3. Enregistrer plusieurs événements d'un seul coup (Subscribers)
 
## Installation

 1. Enregistrer le dossier EventManager dans le dossier _application/libraries._
 2. Dans le fichier _application/config/autoload.php,_ insérez cette ligne `$autoload['libraries'] = array('EventManager' => 'em');` (`$autoload['libraries']` devrait déjà exister).
 
## Utilisation
Vous avez accès à l'EventManager dans vos contrôleurs en utilisant la propriété _em_. Exemple _$this->em->trigger(...)_

#### I. Ecouter un événement

`EventManager::attach($event_name, $callback, $priority = 0)`

Exemple: 

```php
$this->em->attach('posts.create', function (Event $event, $argv = array()) { 
		echo "Un article a été créé !"; 
}, 3);
```

Dans cet exemple, j'écoute l'événement `posts.create`. Si l'événement est déclenché, la fonction en callback va s'exécuter, à savoir écrire "Un article a été créé !".

Remarque: Si j'attache un autre Listener à l'événement `posts.create` mais avec une priorité plus faible, celui-ci sera exécuté après celui ci-dessus. (Ordre décroissant de priorité)

#### II. Déclencher un événement

Pour déclencher une événement, on utilise la méthode trigger:
`EventManager::trigger($event, $target, $argv)`

Event est le nom de l'événement à déclencher

Target peut être:

 - Un objet de type Event
 - Une chaine de caractère correspond à une classe qui hérite de Event
 - Un stdClass
 
Argv représente des arguments à passer à l'event (des informations complémentaires)

Exemple: Je déclenche l'événement `posts.create`

`$this->em->trigger('posts.create', new Event($post), array());`
 
 Ici, j'ai passé un objet de type Event qui contient mon article créé (je pourrai faire un traitement dans mon callback dessus).
 
#### III. Enregistrer un subscriber 

Un subscriber est une classe dans laquelle on va enregistrer plusieurs Listeners.
Pour déclarer un subscriber, il faut créer une classe qui implémente l'interface `SubscriberInterface`.

Cette interface demande qu'on écrive une méthode _registerEvent_ qui va retourner un tableau contenant la liste des événements à écouter ainsi que les méthodes à exécuter lors du déclenchement de ces derniers.

Voici un exemple de tableau retourné par _registerEvent_:

```php
return array('posts.create' => array('doSomething', 'doSomethingElse'));
```

Exemple avec priorités

```php
return array('posts.create' => array(
	array(
		'method' => 'doSomething',
			'priority' => 10
		),
		array(
			'method' => 'doSomethingElse',
			'priority' => 12
		)
	);
```

Pour que le subscriber soit utiliser, il faut le renseigner dans le fichier de configuration de codeigniter: _application/config/config.php_
`$config['event_subscribers'] = array(FirstSubscriber::class, SecondSubscriber::class);`

/!\ Il ne faut pas oublier de charger ces classes
