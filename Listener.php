<?php


class Listener
{

	private $event;
	private $callback;
	private $priority;

	public function __construct($event, $callback, $priority)
	{
		$this->event = $event;
		$this->callback = $callback;
		$this->priority = $priority;
	}

	/**
	 * @return mixed
	 */
	public function getEvent()
	{
		return $this->event;
	}

	/**
	 * @return mixed
	 */
	public function getCallback()
	{
		return $this->callback;
	}

	/**
	 * @return mixed
	 */
	public function getPriority()
	{
		return $this->priority;
	}

}
