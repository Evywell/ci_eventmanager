<?php
require_once "Listener.php";
require_once "Event.php";
require_once "SubscriberLoader.php";

class EventManager
{

	/**
	 * @var array
	 */
	private $listeners;

	/**
	 * @var SubscriberLoader
	 */
	private $loader;

	public function __construct()
	{
		// Initialisation des listeners
		$this->listeners = [];
		$this->loader = new SubscriberLoader($this);
		$ci =& get_instance();
		$subscribers = $ci->config->item('event_subscribers');
		if ($subscribers) {
			$this->loader->loadSubscribers($subscribers);
		}
		// TODO: On charge les subscribers depuis un fichier de configuration
	}

	/**
	 * Attaches a listener to an event
	 *
	 * @param string $event the event to attach too
	 * @param callable $callback a callable function
	 * @param int $priority the priority at which the $callback executed
	 * @return bool true on success false on failure
	 */
	public function attach($event, $callback, $priority = 0)
	{
		$this->doAttach($event, $callback, $priority);
		return true;
	}

	/**
	 * Detaches a listener from an event
	 *
	 * @param string $event the event to attach too
	 * @param callable $callback a callable function
	 * @return bool true on success false on failure
	 */
	public function detach($event, $callback)
	{
		return true;
	}

	/**
	 * Clear all listeners for a given event
	 *
	 * @param  string $event
	 * @return void
	 */
	public function clearListeners($event)
	{
		unset($this->listeners[$event]);
	}

	/**
	 * Trigger an event
	 *
	 * Can accept an EventInterface or will create one if not passed
	 *
	 * @param  string|EventInterface $event
	 * @param  object|string $target
	 * @param  array|object $argv
	 * @return mixed
	 */
	public function trigger($event, $target = null, $argv = array())
	{
		if (!$this->hasListeners($event)) {
			return false;
		}
		usort($this->listeners[$event], function($a, $b) {
			return $a->getPriority() < $b->getPriority();
		});
		/** @var Listener $listener */
		foreach ($this->listeners[$event] as $listener) {
			$callback = $listener->getCallback();
			$eventClass = $target;
			if (is_string($target) && class_exists($target)) {
				$eventClass = new $target($argv);
			}
			call_user_func_array($callback, ['target' => $eventClass, 'argv' => $argv]);
			if (($eventClass instanceof Event) && $eventClass->isStoppedPropagation()) {
				return $eventClass;
			}
		}

		return $target;
	}

	private function doAttach($event, $callback, $priority)
	{
		if (!$this->hasListeners($event)) {
			$this->listeners[$event] = [];
		}

		$this->listeners[$event][] = new Listener($event, $callback, $priority);
	}

	private function hasListeners($event)
	{
		return array_key_exists($event, $this->listeners);
	}

}
