<?php


class Event
{

	private $arg;
	private $stopped;

	public function __construct($arg = [])
	{
		$this->arg = $arg;
		$this->stopped = false;
	}

	/**
	 * @return mixed
	 */
	public function isStoppedPropagation()
	{
		return $this->stopped;
	}

	public function stopPropagation()
	{
		$this->stopped = true;
	}
}
