<?php
require_once "SubscriberInterface.php";

class SubscriberLoader
{

	/**
	 * @var EventManager
	 */
	private $eventManager;

	public function __construct(EventManager $eventManager)
	{
		$this->eventManager = $eventManager;
	}

	public function loadSubscribers($subscribers)
	{
		foreach ($subscribers as $subscriber) {
			$this->loadSubscriber($subscriber);
		}
	}

	public function loadSubscriber($subscriber)
	{
		if (!class_exists($subscriber)) {
			throw new Exception("Class " . $subscriber . " does not exist");
		}
		/** @var SubscriberInterface $instance */
		$instance = new $subscriber();
		if (!$instance instanceof SubscriberInterface) {
			throw new Exception("A subscriber MUST implements SubscriberInterface");
		}

		$events = $instance->registerEvent();
		if (!is_array($events)) {
			throw new Exception("SubscriberInterface::registerEvent MUST return an array");
		}

		foreach ($events as $event => $listener) {
			if (is_array($listener)) {
				array_map(function ($l) use ($event, $instance) {
					$this->attachListener($event, $l, $instance);
				}, $listener);
			} else {
				$this->attachListener($event, $listener, $instance);
			}
		}
	}

	private function attachListener($event, $listener, $subscriber)
	{
		if (is_array($listener)) {
			if (!isset($listener['method'])) {
				throw new RuntimeException("Your event $event is not correctly registered ! You MUST declare a 'method' index");
			}
			$method = $listener['method'];
			$priority = isset($listener['priority']) ? $listener['priority'] : 0;
			return $this->eventManager->attach($event, [$subscriber, $method], $priority);
		}
		return $this->eventManager->attach($event, [$subscriber, $listener]);
	}

}
